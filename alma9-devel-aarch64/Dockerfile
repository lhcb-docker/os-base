FROM gitlab-registry.cern.ch/linuxsupport/alma9-base:latest

# prepare to install everything we need
RUN dnf upgrade --refresh -y \
 && dnf install -y 'dnf-command(config-manager)' \
 && dnf config-manager --set-enabled crb \
 && dnf install -y https://linuxsoft.cern.ch/wlcg/el9/x86_64/wlcg-repo-1.0.0-1.el9.noarch.rpm \
                   https://dl.fedoraproject.org/pub/epel/epel-release-latest-9.noarch.rpm \
 && dnf clean all && rm -rf /var/cache/dnf

# get HEP_OSlibs
# HACK: Add libzstd-devel while we wait for it to be added to HEP_OSlibs/LCG
RUN dnf install -y HEP_OSlibs libzstd-devel && dnf clean all && rm -rf /var/cache/dnf

# extra useful libraries
RUN dnf install -y \
      openssl-devel libXpm-devel procmail git git-lfs \
      strace \
      symlinks libxslt \
      expat-devel openmotif-devel mesa-libGLU-devel \
      libgit2-devel numactl-devel \
      glibc-static \
      bzip2 zip unzip xz-devel bash-completion \
      tmux nano vim emacs \
      wget \
      xrootd-client lcov ccache \
 && dnf clean all && rm -rf /var/cache/dnf

COPY eos9-aarch64.repo /etc/yum.repos.d/eos9-aarch64.repo
RUN dnf install -y \
      eos-client \
 && dnf clean all && rm -rf /var/cache/dnf

# Enable SHA1 for certificate (see https://twiki.cern.ch/twiki/bin/view/LCG/WLCGOpsMinutes230302#Middleware_News)
RUN dnf install -y \
      crypto-policies-scripts \
 && update-crypto-policies --set DEFAULT:SHA1 \
 && dnf clean all && rm -rf /var/cache/dnf
